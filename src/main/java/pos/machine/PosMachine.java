package pos.machine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PosMachine {
    public List<ReceiptItem> decodeToItems(List<String> barcodes){
        HashMap<String,Integer> barcodeMap =new HashMap<>();

        for (String barcode:barcodes){
            if (barcodeMap.containsKey(barcode)){
                barcodeMap.put(barcode,barcodeMap.get(barcode)+1);
            }else{
                barcodeMap.put(barcode,1);
            }
        }

        List<ReceiptItem> receiptitems = new ArrayList<>();
        for (String key:barcodeMap.keySet()){
            Item item = findByBarcode(key);
            if (item != null){
                String name = item.getName();
                int price = item.getPrice();
                int subtotal = price * barcodeMap.get(key);
                ReceiptItem receiptItem = new ReceiptItem(name,barcodeMap.get(key),price,subtotal);
                receiptitems.add(receiptItem);
            }
        }
        return receiptitems;
    }

    public Item findByBarcode(String barcode) {
        List<Item> items = ItemsLoader.loadAllItems();
        Item currentItem = null;
        for (Item item:items){
            if (item.getBarcode().equals(barcode)){
                currentItem = item;
            }
        }
        return currentItem;
    }

    public int calculateTotalPrice(List<ReceiptItem> receiptItems){
        int totalPrice = 0;
        for (ReceiptItem receiptItem:receiptItems) {
            totalPrice += receiptItem.getSubTotal();
        }
        return totalPrice;
    }

    public Receipt calculateCost(List<ReceiptItem> receiptItems) {
        int totalPrice = calculateTotalPrice(receiptItems);
        return new Receipt(receiptItems,totalPrice);
    }


    public String generateItemsReceipt(Receipt receipt){
        String itemsReceipt = "";
        for (ReceiptItem receipt1:receipt.getReceiptItems()){
            itemsReceipt += receipt1.toString();
        }
        return itemsReceipt;

    }

    public String generateReceipt(String itemsReceipt,int totalPrice){
        String receipt = "***<store earning no money>Receipt***\n" + itemsReceipt +
                "----------------------\n" + "Total: "+totalPrice+" (yuan)\n" +
                "**********************";

        return receipt;
    }

    public String renderReceipt(Receipt receipt){
        String itemsReceipt = generateItemsReceipt(receipt);
        String receipt1 = generateReceipt(itemsReceipt,receipt.getTotalPrice());
        return receipt1;
    }

    public String printReceipt(List<String> barcodes) {
        List<ReceiptItem> receiptItems = decodeToItems(barcodes);
        Receipt receipt = calculateCost(receiptItems);
        return renderReceipt(receipt);
    }



}
