# Daily Report (2023/07/11)
# O (Objective): 
## 1. What did we learn today?
### We learned tasking,context map,function naming method and git basic today.

## 2. What activities did you do? 
### I worked with my team members to draw context map and do some java code exercises.

## 3. What scenes have impressed you?
### I have been impressed by the help in configuring the computer environment from Teacher Fu and Teacher Wen.
### In addition,I was impressed by Gary's sharing of his internship experience in OOCL in class. He said that the BA needed to hold many meetings in the project team to follow up the progress of tasks. It makes me more aware of my responsibilities going forward. 
---------------------

# R (Reflective): 
## Please use one word to express your feelings about today's class.
---------------------
### Intensive.

# I (Interpretive):
## What do you think about this? What was the most meaningful aspect of this activity?
---------------------
### I think the most meaningful aspect of this activity is building a context with my team members,because it was also a good exercise for my communication skills.
### I am also grateful to the TW teachers,my team members Jaden and Garrick for their help today, which enabled me to solve the problems I encountered in my homework.


# D (Decisional): 
## Where do you most want to apply what you have learned today? What changes will you make?
---------------------
### I want to apply the context map to the following requirement analysis so that I can write more reusable code.
### Compared to the past, I will use Java stream instead of loop when I write the functions and shift my programming thinking to make my code more encapsulated and exclusive.





